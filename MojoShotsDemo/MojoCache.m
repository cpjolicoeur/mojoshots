//
//  MojoCache.m
//  MojoShotsDemo
//
//  Created by Craig Jolicoeur on 10/31/13.
//  Copyright (c) 2013 Craig Jolicoeur. All rights reserved.
//

#import "MojoCache.h"

@implementation MojoCache

- (NSDictionary *)substitutionPaths {
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"mojo_logo.png", @"http://assets2.dribbble.com/assets/logo-bw-1a9f2cf0c682a1c1741f5133211f188d.gif",
            @"mojo_logo.png", @"http://assets1.dribbble.com/assets/logo-small-2x-8b3454458efece701f3545c64b5f0846.png",
            @"mojotech_marker.png", @"http://assets0.dribbble.com/assets/ball-519be06016e46dc0c1f27b2a0b3c56e8.png",
            @"mojotech_marker_2x.png", @"http://assets0.dribbble.com/assets/ball-2x-96be706f8c39190c701e82b9a4177760.png",
            nil];
}

- (NSString *)mimeTypeForPath:(NSString *)originalPath {
    // we are only using PNG images for now
    return @"image/png";
}

- (NSCachedURLResponse *)cachedResponseForRequest:(NSURLRequest *)request {
    // Get request path
    NSString *pathString = [[request URL] absoluteString];
    
    // see if we have a substition file for this path
    NSString *substitutionFileName = [[self substitutionPaths] objectForKey:pathString];
    if (!substitutionFileName) {
        // no substitution file, return the default cache response
        return [super cachedResponseForRequest:request];
    }
    
    // If we already made a cached entry for this path, just use it
    NSCachedURLResponse *cachedResponse = [cachedResponses objectForKey:pathString];
    if (cachedResponse) {
        return cachedResponse;
    }
    
    // Get the path to the substitute file
    NSString *substitutionFilePath = [[NSBundle mainBundle] pathForResource:[substitutionFileName stringByDeletingPathExtension]
                                                                     ofType:[substitutionFileName pathExtension]];
    NSAssert(substitutionFilePath, @"File %@ in substitutionPaths didn't exist", substitutionFilePath);
    
    // Load the substitute data
    NSData *data = [NSData dataWithContentsOfFile:substitutionFilePath];
    
    // Create the cacheable response
    NSURLResponse *response = [[NSURLResponse alloc] initWithURL:[request URL]
                                                        MIMEType:[self mimeTypeForPath:pathString]
                                           expectedContentLength:[data length]
                                                textEncodingName:nil];
    cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:response data:data];
    
    // Add the response to our cache dictionary
    if (!cachedResponses) {
        cachedResponses = [[NSMutableDictionary alloc] init];
    }
    [cachedResponses setObject:cachedResponse forKey:pathString];
    
    return cachedResponse;
}

- (void)removeCachedResponseForRequest:(NSURLRequest *)request {
    NSString *pathString = [[request URL] path];
    if ([cachedResponses objectForKey:pathString]) {
        [cachedResponses removeObjectForKey:pathString];
    } else {
        [super removeCachedResponseForRequest:request];
    }
}

@end
