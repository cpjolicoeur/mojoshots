//
//  AppDelegate.h
//  MojoShotsDemo
//
//  Created by Craig Jolicoeur on 10/31/13.
//  Copyright (c) 2013 Craig Jolicoeur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
