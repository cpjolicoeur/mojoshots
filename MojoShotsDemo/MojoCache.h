//
//  MojoCache.h
//  MojoShotsDemo
//
//  Created by Craig Jolicoeur on 10/31/13.
//  Copyright (c) 2013 Craig Jolicoeur. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MojoCache : NSURLCache
{
    NSMutableDictionary *cachedResponses;
}

@end
