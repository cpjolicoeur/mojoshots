//
//  ShotsTableViewController.m
//  MojoShotsDemo
//
//  Created by Craig Jolicoeur on 10/31/13.
//  Copyright (c) 2013 Craig Jolicoeur. All rights reserved.
//

#import "ShotsTableViewController.h"
#import "ShotsWebViewController.h"
#import <AFNetworking.h>
#import <UIImageView+AFNetworking.h>

@interface ShotsTableViewController () {
    NSMutableArray *_shots;
    NSMutableDictionary *_cachedImages;
}
@end

@implementation ShotsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"MojoShots";
        _shots = [NSMutableArray array];
        _cachedImages = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AFHTTPRequestOperationManager *afmanager = [AFHTTPRequestOperationManager manager];
    [afmanager GET:@"http://api.dribbble.com/shots/popular?page=1" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"JSON: %@", responseObject[@"shots"]);
        [_shots addObjectsFromArray:responseObject[@"shots"]];
        [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Methods

- (void)updateRowAtIndexPath:(NSIndexPath *)indexPath withImage:(UIImage *)image
{
    [_cachedImages setObject:image forKey:[_shots objectAtIndex:indexPath.row][@"id"]];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1 == section ? 1 : [_shots count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    if (indexPath.section == 1) {
        cell.textLabel.text = @"Custom Protocol Demo Page";
        cell.imageView.image = nil;
    } else {
        cell.textLabel.text = [_shots objectAtIndex:indexPath.row][@"player"][@"username"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if ([_cachedImages objectForKey:[_shots objectAtIndex:indexPath.row][@"id"]]) {
            cell.imageView.image = [_cachedImages objectForKey:[_shots objectAtIndex:indexPath.row][@"id"]];
        } else {
            [cell.imageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[_shots objectAtIndex:indexPath.row][@"image_teaser_url"]]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                [self updateRowAtIndexPath:indexPath withImage:image];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                NSLog(@"Error: %@", error);
            }];
        }
    }

    return cell;
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Create the next view controller.
    ShotsWebViewController *webVC;
    if (indexPath.section == 0) {
        webVC = [[ShotsWebViewController alloc] initWithShot:[_shots objectAtIndex:indexPath.row]];
    } else {
        webVC = [[ShotsWebViewController alloc] initWithShot:nil];
    }
    [self.navigationController pushViewController:webVC animated:YES];
}

@end
