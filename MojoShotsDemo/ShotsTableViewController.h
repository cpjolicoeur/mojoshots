//
//  ShotsTableViewController.h
//  MojoShotsDemo
//
//  Created by Craig Jolicoeur on 10/31/13.
//  Copyright (c) 2013 Craig Jolicoeur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShotsTableViewController : UITableViewController

- (void)updateRowAtIndexPath:(NSIndexPath *)indexPath withImage:(UIImage *)image;

@end

