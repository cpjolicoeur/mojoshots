//
//  ShotsWebViewController.m
//  MojoShotsDemo
//
//  Created by Craig Jolicoeur on 10/31/13.
//  Copyright (c) 2013 Craig Jolicoeur. All rights reserved.
//

#import "ShotsWebViewController.h"

@interface ShotsWebViewController ()

@end

@implementation ShotsWebViewController

- (id)initWithShot:(NSDictionary *)shot {
    self = [super initWithNibName:@"ShotsWebViewController" bundle:nil];
    if (self) {
        self.title = @"Shot on Dribbble";
        self.shot = shot;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (self.shot) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.shot[@"url"]]]];
    } else {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://craigjolicoeur.com/demo/mojoshots.html"]]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (![[request URL].scheme isEqualToString:@"http"]) {
        [self performSelector:@selector(showCamera) withObject:nil afterDelay:0.1];
        return FALSE;
    }
    return TRUE;
}

#pragma mark - Camera Methods & Delegate

- (void)showCamera {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *camera = [[UIImagePickerController alloc] init];
        camera.sourceType = UIImagePickerControllerSourceTypeCamera;
        camera.delegate = self;
        [self presentViewController:camera animated:YES completion:NULL];
    } else {
        NSLog(@"Camera not available on this device");
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Camera Picker finished: %@", info);
}

@end
